const express = require('express');
const bodyParser = require('body-parser'); 
const url = require('url');  
const querystring = require('querystring');  
const port = 3000;
const child = require('child_process').execFile;
const fs = require('fs');
const executablePath = 'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe';


let path = 'D:/konydeployreport/deploy_status.txt';  
let buffer = new Buffer('Those who wish to follow me\nI welcome with my hands\nAnd the red sun sinks at last');
let app = express();
app.use(bodyParser.urlencoded({ extended: false }));  
app.use(bodyParser.json());

app.post('/mfdeploy', (req, res) => {

    child(executablePath, ['file:///D:/automate_script/SimulateKonyDeployWithDeployTest.html?direct=1'], function (err, data) {
        if (err) {
            console.error(err);
            return;
        }

        console.log(data.toString());
    });
	
	// loop, 30secs, check if report file exists
	// if yes, read result
	// delete file
	// send response to node client
	
    res.send('mfDeploy Done');
});


app.get('/createReport', (req, res) => {
	let deployDate = req.query.deploydate;
	let deployStatus = req.query.deploystatus;
	let deployBuffer = new Buffer('Deploy Date : ' + deployDate + ' | ' + deployStatus  + '\r\n');
	
	createReportFile(deployBuffer);
	res.send('createReport Done');
	
});

function createReportFile(deployBuffer) {
	
	fs.open(path, 'w', function(err, fd) {  
    if (err) {
        throw 'could not open file: ' + err;
    }

		fs.appendFile(fd, deployBuffer,  {'flag':'as+'},  function(err) {
        if (err) throw 'error writing file: ' + err;
        fs.close(fd, function() {
            console.log('wrote the file successfully');
        });
    });
});

 }

app.listen(port, () => console.log('Example app listening on port ' + port));